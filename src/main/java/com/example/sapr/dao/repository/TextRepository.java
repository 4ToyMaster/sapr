package com.example.sapr.dao.repository;

import com.example.sapr.dao.entity.Text;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TextRepository extends JpaRepository<Text, Long> {

}
