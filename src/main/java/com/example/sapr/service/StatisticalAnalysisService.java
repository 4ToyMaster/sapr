package com.example.sapr.service;

import com.example.sapr.exceptions.AnalysisException;
import static java.lang.Math.log;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class StatisticalAnalysisService {

  public LinkedHashMap<String, Integer> getFrequencyDictonary(String inputText) {
    List<String> words = splitText(inputText);
    HashMap<String, Integer> result = new HashMap<>();
    words.stream()
        .forEach(
            s -> {
              if (s != null && s.length() > 0) {
                if (result.containsKey(s)) {
                  result.put(s, result.get(s) + 1);
                } else {
                  result.put(s, 1);
                }
              }
            }
        );
    return result.entrySet().stream()
        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
  }

  public String getMutualInformation(String inputText, String phrase) throws AnalysisException {
    String result = "Статистический метод Mutual Information";
    String[] phraseParts = phrase.toLowerCase().split(" ");
    if (phraseParts == null || phraseParts.length != 2) {
      throw new AnalysisException("Некорректная биграмма: " + phrase);
    }
    List<String> words = splitText(inputText);
    List<String> inputsAll = splitText(inputText);
    List<String> inputsNotPhrase = splitText(inputText.toLowerCase().replaceAll(phrase.toLowerCase(), "."));
    int Fxy = (inputsAll.size() - inputsNotPhrase.size()) / 2;
    result += "F(xy) = " + Fxy + "\n";
    int Fx = 0, Fy = 0;
    LinkedHashMap<String, Integer> freqDictonary = getFrequencyDictonary(inputText);
    if (freqDictonary.containsKey(phraseParts[0])) {
      Fx = freqDictonary.get(phraseParts[0]);
    }

    if (freqDictonary.containsKey(phraseParts[1])) {
      Fy = freqDictonary.get(phraseParts[1]);
    }
    if (Fx == 0 || Fy == 0) {
      throw new AnalysisException("Вычисленная частота одного из слов равна 0: F(x)=" + Fx + ", F(y)=" + Fy);
    }
    result += "F(x) = " + Fx + "\n";
    result += "F(y) = " + Fy + "\n";

    Double Mi = Math.pow(((double)(Fxy * words. size()) / (Fx * Fy)), 2);
    result += "Mi = " + Mi + "\n";

    if (Mi < 0) {
      return result += "Каждое из слов встречается лишь в тех позициях, в которых не встречается другое";
    }
    if (Mi > 1) {
      return result += "Значимо";
    }
    return result += "Не значимо";
  }

  public String getTScore(String inputText, String phrase) throws AnalysisException {
    String result = "Статистический метод T-Score";
    String[] phraseParts = phrase.toLowerCase().split(" ");
    if (phraseParts == null || phraseParts.length != 2) {
      throw new AnalysisException("Некорректная биграмма: " + phrase);
    }
    List<String> words = splitText(inputText);
    List<String> inputsAll = splitText(inputText);
    List<String> inputsNotPhrase = splitText(inputText.toLowerCase().replaceAll(phrase.toLowerCase(), "."));
    int Fxy = (inputsAll.size() - inputsNotPhrase.size()) / 2;
    result += "F(xy) = " + Fxy + "\n";
    int Fx = 0, Fy = 0;
    LinkedHashMap<String, Integer> freqDictonary = getFrequencyDictonary(inputText);
    if (freqDictonary.containsKey(phraseParts[0])) {
      Fx = freqDictonary.get(phraseParts[0]);
    }

    if (freqDictonary.containsKey(phraseParts[1])) {
      Fy = freqDictonary.get(phraseParts[1]);
    }

    result += "F(x) = " + Fx + "\n";
    result += "F(y) = " + Fy + "\n";
    Double TS = (Fxy - (Fx * Fy) / words.size()) / (Math.pow(Fxy, 2));
    result += "T-Score = " + TS + "\n";

    return result;
  }

  public String getLogLikelihood(String inputText, String phrase) throws AnalysisException {
    String result = "Статистический метод Log-Likelihood";
    String[] phraseParts = phrase.toLowerCase().split(" ");
    if (phraseParts == null || phraseParts.length != 2) {
      throw new AnalysisException("Некорректная биграмма: " + phrase);
    }
    List<String> inputsAll = splitText(inputText);
    List<String> inputsNotPhrase = splitText(inputText.toLowerCase().replaceAll(phrase.toLowerCase(), "."));
    int a = (inputsAll.size() - inputsNotPhrase.size()) / 2;

    int b = 0;
    int c = 0;
    Integer d = 0;
    LinkedHashMap<String, Integer> freqDictonary = getFrequencyDictonary(inputText);
    if (freqDictonary.containsKey(phraseParts[0])) {
      b = freqDictonary.get(phraseParts[0]);
    }

    if (freqDictonary.containsKey(phraseParts[1])) {
      c = freqDictonary.get(phraseParts[1]);
    }

    d = freqDictonary.entrySet().stream()
        .filter(entry -> (entry.getKey() != phraseParts[0] && entry.getKey() != phraseParts[1]))
        .mapToInt(entry -> entry.getValue())
        .sum();
    Double logLike = a * log(a + 1) + b * log(b + 1) + c * log(c + 1) + d * log(d + 1) -
        (a + b) * log(a + b + 1) - (a + c) * log(a + c + 1) - (b + d) * log(b + d + 1) -
        (c + d) * log(c + d + 1) + (a + b + c + d) * log(a + b + c + d + 1);

    result += "a = " + a + "\n";
    result += "b = " + b + "\n";
    result += "c = " + c + "\n";
    result += "d = " + d + "\n";
    result += "Log-Likelihood = " + logLike;
    return result;
  }

  public String getTfIdf(String inputText, String phrase, String[] corpus) throws AnalysisException {
    String result = "Статистический метод TF*IDF";
    String[] phraseParts = phrase.toLowerCase().split(" ");
    if (phraseParts == null || phraseParts.length != 1) {
      throw new AnalysisException("Некорректная биграмма: " + phrase);
    }
    List<String> words = splitText(inputText);
    LinkedHashMap<String, Integer> freqDictonary = getFrequencyDictonary(inputText);
    //TF
    int fx = freqDictonary.get(phrase);
    result += "f(x) = " + fx + "\n";
    result += "N = " + words.size() + "\n";
    double TF = (double)fx / (double)words.size();
    result += "TF = " + TF + "\n";
    //IDF
    result += "N = " + corpus.length + "\n";
    int b = Arrays.stream(corpus).filter(s -> s.contains(phrase)).collect(Collectors.toList()).size();
    if (b == 0) {
      throw new AnalysisException("Корпус не содержит упоминаний слова `" + phrase + "`. b = 0");
    }
    result += "b = " + b + "\n";
    double IDF = log((corpus.length - b) / b);
    result += "IDF = " + IDF + "\n";
    result += "TF*IDF = " + (TF * IDF);

    return result;
  }

  private List<String> splitText(String inputText) {
    String text = inputText.replaceAll("[^a-zA-Z-а-яА-я\\s]", "").replaceAll("\n|-", "").toLowerCase();
    return Arrays.asList(text.split(" "));
  }

}
