package com.example.sapr.service;

import com.example.sapr.constants.Url.Analysis;
import com.example.sapr.exceptions.AnalysisException;
import java.io.File;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import ru.stachek66.nlp.mystem.holding.Factory;
import ru.stachek66.nlp.mystem.holding.MyStem;
import scala.Option;
import ru.stachek66.nlp.mystem.holding.MyStemApplicationException;
import ru.stachek66.nlp.mystem.holding.Request;
import ru.stachek66.nlp.mystem.model.Info;
import scala.collection.JavaConversions;

@Service
public class MorphologicalAnalysisService {

  private final static MyStem mystemAnalyzer =
      new Factory("-igd --eng-gr --format json --weight")
          .newMyStem("3.0", Option.<File>empty()).get();

  public LinkedHashMap getFrequencyDictonary(String text) throws AnalysisException {
    try {
      final Iterable<Info> analysis =
          JavaConversions.asJavaIterable(
              mystemAnalyzer
                  .analyze(Request.apply(text))
                  .info()
                  .toIterable());
      LinkedHashMap<String, Integer> result = new LinkedHashMap<>();
      for (final Info info : analysis) {
        if (info.lex().nonEmpty()) {
          String lex = info.lex().get();
          if (result.containsKey(lex)) {
            result.put(lex, result.get(lex) + 1);
          } else {
            result.put(lex, 0);
          }
        }
      }
      return result.entrySet().stream()
          .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
          .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
              (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    } catch (MyStemApplicationException ex) {
      throw new AnalysisException(ex.getMessage());
    }
  }

}
