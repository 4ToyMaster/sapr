package com.example.sapr.service;

import com.example.sapr.Cluster.KlasterSystem;
import com.example.sapr.exceptions.AnalysisException;
import java.util.LinkedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClusterAnalysisService {

  @Autowired
  private MorphologicalAnalysisService morphologicalAnalysisService;

  public String run(String text){
    LinkedHashMap<String, Integer> frequency = null;
    try {
      frequency = morphologicalAnalysisService.getFrequencyDictonary(text);
    } catch (AnalysisException e) {
      return e.getMessage();
    }
    KlasterSystem KlSys = new KlasterSystem(2, 1000, 0.001f, 1.6f, frequency);

    StringBuffer result = new StringBuffer();
    try {
      LinkedHashMap<String, String> dictonary = KlSys.run(frequency);
      result.append("Clusters:\n");
      for (float val : KlSys.CenKlast) {
        result.append("Cluster = " + val + ";\n");
      }
      dictonary.entrySet().stream().forEach(
          entry -> {
            result.append(entry.getKey() + "  :  " + entry.getValue() + ";\n");
          }
      );
    } catch (AnalysisException e) {
      return e.getMessage();
    }
    return result.toString();

  }

}
