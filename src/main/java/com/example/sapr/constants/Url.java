package com.example.sapr.constants;

public interface Url {

  String ROOT = "api";

  interface Analysis {

    String PART = "analysis";

    String FULL = ROOT + "/" + PART;

    interface StatisticalAnalysis {

      String PART = "statistical";

      String FULL = Analysis.FULL + "/" + PART;
    }

    interface MorphologicalAnalysis {

      String PART = "morphological";

      String FULL = Analysis.FULL + "/" + PART;
    }

    interface ClusterAnalysis {
      String PART = "cluster";

      String FULL = Analysis.FULL + "/" + PART;
    }

  }

}
