package com.example.sapr.Cluster;

import com.example.sapr.exceptions.AnalysisException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class KlasterSystem {

  float[][] StPr;
  Integer[] objecti;
  float mi; //параметр q
  float ei;
  int Iter;
  int countKlast;
  int countObj;
  public float[] CenKlast;
  Random r = new Random();

  public KlasterSystem(int ck, int I, float e, float m, LinkedHashMap<String, Integer> obj) {
    countKlast = ck;
    mi = m;
    ei = e;
    Iter = I;
    CenKlast = new float[countKlast];
    objecti = obj.values().toArray(new Integer[0]);
    countObj = objecti.length;
  }

  boolean Klaster_CheckStPr() {
    for (int i = 0; i < countObj; i++) {
      int t = -1;
      for (int j = 0; j < countKlast; j++) {
        if (StPr[i][j] == 1) {
          t = j;
          break;
        }
      }
      if (t >= 0) {
        for (int j = 0; j < countKlast; j++) {
          if (j != t) {
            StPr[i][j] = 0;
          }
        }
      }
      float sum = 0;
      for (int j = 0; j < countKlast; j++) {
        sum = sum + (StPr[i][j]);
      }

      while (sum - 1 >= 0.01) {
        float max = 0;
        int ti = 0;
        for (int j = 0; j < countKlast; j++) {
          if (StPr[i][j] > max) {
            max = StPr[i][j];
            ti = j;
          }
        }
        float razn = max - sum + 1;
        if (razn >= 0) {
          StPr[i][ti] = razn;
          sum = 0;
        }
        if (razn < 0) {
          StPr[i][ti] = 0;
          sum = sum - max;
        }
      }
      sum = 0;
      for (int j = 0; j < countKlast; j++) {
        sum = sum + (StPr[i][j]);
      }
    }
    return true;
  }

  void InitFirstFunction() {
    StPr = new float[countObj][countKlast];
    for (int i = 0; i < countObj; i++) {
      for (int j = 0; j < countKlast; j++) {
        StPr[i][j] = (float) r.nextDouble();
      }
    }
    Klaster_CheckStPr();
  }

  float Klaster_CountRasst(float x, float c) {
    return (float) (Math.abs(x - c));
  }

  float Klaster_CountOcF(int Znach, int Klast) {
    float sum = 0f;
    for (int i = 0; i < Znach; i++) {
      for (int j = 0; j < Klast; j++) {
        mi = sum + (float) (Math.pow(StPr[i][j], mi) * Klaster_CountRasst(objecti[i], CenKlast[j]));
      }
    }
    return sum;
  }

  float Klaster_CountNewU(int t, int j) {
    float u = 0f;
    if (Klaster_CountRasst(objecti[t], CenKlast[j]) == 0) {
      return 1;
    } else {
      for (int i = 0; i < countKlast; i++) {
        if
        (Klaster_CountRasst(objecti[t], CenKlast[i]) == 0) {
          return 1;
        } else {
          u = u + (float) Math.pow(Klaster_CountRasst
              (objecti[t], CenKlast[j]) /
              Klaster_CountRasst(objecti[t], CenKlast[i]), 2 / (mi - 1));
        }
      }

      if (u != 0) {
        u = 1 / u;
      }
      return u;
    }
  }

  float Count_CentKlast(int j) {
    float c = 0f;
    float c1 = 0f;
    for (int i = 0; i < countObj; i++) {
      c = c + (float) (Math.pow(StPr[i][j], mi)) * objecti[i];
      c1 = c1 + (float) (Math.pow(StPr[i][j], mi));
    }
    return c / c1;
  }

  public LinkedHashMap<String, String> run(Map<String, Integer> objects) throws AnalysisException {
    int Shag = 0;
    float epx = 1000f;
    float OldF = 0f;
    float NewF = 0f;
    LinkedHashMap<String, String> result = new LinkedHashMap<>();

    this.InitFirstFunction();

    for (int j = 0; j < this.countKlast; j++) {
      this.CenKlast[j] = this.Count_CentKlast(j);
    }
    this.Klaster_CountOcF(this.countObj, this.countKlast);
    while ((Shag < this.Iter) && (epx > this.ei)) //main cicle
    {

      Shag++;
      for (int j = 0; j < this.countKlast; j++) {
        this.CenKlast[j] = this.Count_CentKlast(j);
      }
      for (int ii = 0; ii < this.countObj; ii++) {
        for (int j = 0; j < this.countKlast; j++) {
          this.StPr[ii][j] = this.Klaster_CountNewU(ii, j);
        }
      }
      if (!this.Klaster_CheckStPr()) {
        throw new AnalysisException("error");
      }
      NewF = this.Klaster_CountOcF(this.countObj, this.countKlast);
      if (Shag > 1) {
        epx = Math.abs(OldF - NewF);
      }
      OldF = NewF;
    } //main cicle

    for (int i = 0; i < this.countObj; i++) {
      float maxStPr = this.StPr[i][0];
      int numk = 0;
      for (int j = 0; j < this.countKlast; j++) {
        if (maxStPr < this.StPr[i][j]) {
          maxStPr = this.StPr[i][j];
          numk = j;
        }
        result.put(getName(objects, objecti[i], j == 1), "Частота =" + objecti[i] + "; Степень = " + StPr[i][j]);
      }
    }
    return result;
  }

  String getName(Map<String, Integer> o, int g, boolean del) {
    LinkedHashMap fillter = o.entrySet().stream()
        .filter(x -> x.getValue() == g)
        .collect(Collectors
            .toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    List<String> keys = new ArrayList<>(fillter.keySet());
    String s = (fillter != null && fillter.size() >= 1) ? keys.get(0) : "";
    if (del) {
      o.remove(s);
    }
    return s;
  }

}


