package com.example.sapr.rest;

import com.example.sapr.constants.Url.Analysis.StatisticalAnalysis;
import com.example.sapr.exceptions.AnalysisException;
import com.example.sapr.service.StatisticalAnalysisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.LinkedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Статистический анализ", description = "API для статического анализа текста")
@RestController(value = StatisticalAnalysis.FULL)
public class StatisticalAnalysisController {

  @Autowired
  StatisticalAnalysisService statisticalAnalysisService;

  @ApiOperation(value = "Частотный словарь")
  @PostMapping(StatisticalAnalysis.FULL + "/dictonary")
  public String run(@RequestParam String text) {
    LinkedHashMap<String, Integer> dictonary = statisticalAnalysisService.getFrequencyDictonary(text);
    StringBuffer result = new StringBuffer("Частотный словарь");
    dictonary.entrySet().stream().forEach(entry -> result.append("word: " + entry.getKey() + " freq = " + entry.getValue() + "\n"));
    return result.toString();
  }

  @ApiOperation(value = "Анализ биграммы Mutual Information")
  @PostMapping(StatisticalAnalysis.FULL + "/mutual-info")
  public String getMutualInformation(@RequestParam String text, @RequestParam String phrase) {
    try {
      return statisticalAnalysisService.getMutualInformation(text, phrase);
    } catch (AnalysisException e) {
      return e.getMessage();
    }
  }

  @ApiOperation(value = "Анализ биграммы T-Score")
  @PostMapping(StatisticalAnalysis.FULL + "/t-score")
  public String getTScore(@RequestParam String text, @RequestParam String phrase) {
    try {
      return statisticalAnalysisService.getTScore(text, phrase);
    } catch (AnalysisException e) {
      return e.getMessage();
    }
  }

  @ApiOperation(value = "Анализ биграммы LogLikelihood")
  @PostMapping(StatisticalAnalysis.FULL + "/log-likelihood")
  public String getLogLikelihood(@RequestParam String text, @RequestParam String phrase) {
    try {
      return statisticalAnalysisService.getLogLikelihood(text, phrase);
    } catch (AnalysisException e) {
      return e.getMessage();
    }
  }

  @ApiOperation(value = "Анализ монограммы TF-IDF")
  @PostMapping(StatisticalAnalysis.FULL + "/tf-idf")
  public String getTfIdf(@RequestParam String text, @RequestParam String phrase, @RequestParam String[] corpus) {
    try {
      return statisticalAnalysisService.getTfIdf(text, phrase, corpus);
    } catch (AnalysisException e) {
      return e.getMessage();
    }
  }
}
