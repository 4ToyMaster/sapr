package com.example.sapr.rest;

import com.example.sapr.constants.Url.Analysis.MorphologicalAnalysis;
import com.example.sapr.exceptions.AnalysisException;
import com.example.sapr.service.MorphologicalAnalysisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Морфологический анализ", description = "API для морфологическиго анализа текста")
@RestController(value = MorphologicalAnalysis.FULL)
public class MorphologicalAnalysisController {

  @Autowired
  private MorphologicalAnalysisService morphologicalAnalysisService;

  @ApiOperation(value = "Частотный словарь")
  @PostMapping(MorphologicalAnalysis.FULL + "/dictonary")
  public String run(@RequestBody String text) {
    StringBuffer result = new StringBuffer();
    try {
      LinkedHashMap<String, Integer> dictonary = morphologicalAnalysisService.getFrequencyDictonary(text);
      dictonary.entrySet().stream().forEach(
          entry -> {
            result.append(entry.getKey() + "  :  " + entry.getValue() + ";\n");
          }
      );
    } catch (AnalysisException e) {
      return e.getMessage();
    }
    return result.toString();
  }
}
