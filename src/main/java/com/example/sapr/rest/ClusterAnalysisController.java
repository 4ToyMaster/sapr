package com.example.sapr.rest;

import com.example.sapr.constants.Url.Analysis.ClusterAnalysis;
import com.example.sapr.service.ClusterAnalysisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Кластерный анализ", description = "API для кластерного анализа текста")
@RestController(value = ClusterAnalysis.FULL)
public class ClusterAnalysisController {

  @Autowired
  private ClusterAnalysisService clusterAnalysisService;

  @ApiOperation(value = "Частотный словарь")
  @PostMapping(ClusterAnalysis.FULL + "/dictonary")
  public String run(@RequestParam String text) {
    return clusterAnalysisService.run(text);
  }

}
