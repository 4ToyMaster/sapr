package com.example.sapr.exceptions;

public class AnalysisException extends Exception {

  public AnalysisException(String message) {
    super(message);
  }
}
