var base_url = 'http://localhost:8080/api/';
var corpusArray = [];

function getAnalysis() {
  var inputText = document.querySelector('#inputText').value;
  var phrase = document.querySelector('#phrase').value;

  //type of analysis
  var statistic = document.querySelector('#StatAnalysis').checked;
  var morph = document.querySelector('#MorphAnalysis').checked;
  var cluster = document.querySelector('#ClusterAnalysis').checked;

  //type of statistic analysis
  var mi = document.querySelector('#mutalInfo').checked;
  var ll = document.querySelector('#log-likelihood').checked;
  var ts = document.querySelector('#tscore').checked;
  var ti = document.querySelector('#tfidf').checked;
  var freq = document.querySelector('#freqDictonary').checked;

  if (statistic) {
    if (freq) {
      $.ajax({
        cache: false,
        type: "POST",
        async: false,
        url: base_url + "analysis/statistical/dictonary",
        data: 'text=' + inputText,
        success: function (data) {
          updateResult(data);
        },
        error: function (error) {
          updateResult(error.responseText.toString())
        }
      });
    }
    if (mi) {
      $.ajax({
        cache: false,
        type: "POST",
        async: false,
        url: base_url + "analysis/statistical/mutual-info",
        data: 'text=' + inputText + '&phrase=' + phrase,
        success: function (data) {
          updateResult(data);
        },
        error: function (error) {
          updateResult(error.responseText.toString())
        }
      });
    }
    if (ll) {
      $.ajax({
        cache: false,
        type: "POST",
        async: false,
        url: base_url + "analysis/statistical/log-likelihood",
        data: 'text=' + inputText + '&phrase=' + phrase,
        success: function (data) {
          updateResult(data);
        },
        error: function (error) {
          updateResult(error.responseText.toString());
        }
      });
    }
    if (ts) {
      $.ajax({
        cache: false,
        type: "POST",
        async: false,
        url: base_url + "analysis/statistical/t-score",
        data: 'text=' + inputText + '&phrase=' + phrase,
        success: function (data) {
          updateResult(data);
        },
        error: function (error) {
          updateResult(error.responseText.toString());
        }
      });
    }
    if (ti) {
      var param = "";
      corpusArray.forEach(
          function (value) {
            param += '&corpus=' + value;
          }
      );
      $.ajax({
        cache: false,
        type: "POST",
        async: false,
        url: base_url + "analysis/statistical/tf-idf",
        data: 'text=' + inputText + '&phrase=' + phrase + param,
        success: function (data) {
          updateResult(data);
        },
        error: function (error) {
          updateResult(error.responseText.toString());
        }
      });
    }
  }

  if (morph) {
    $.ajax({
      cache: false,
      type: "POST",
      async: false,
      url: base_url + "analysis/morphological/dictonary",
      data: 'text=' + inputText,
      success: function (data) {
        updateResult(data);
      },
      error: function (error) {
        updateResult(error.responseText.toString());
      }
    });
  }
  if (cluster) {
    $.ajax({
      cache: false,
      type: "POST",
      async: false,
      url: base_url + "analysis/cluster/dictonary",
      data: 'text=' + inputText,
      success: function (data) {
        updateResult(data);
      },
      error: function (error) {
        updateResult(error.responseText.toString());
      }
    });
  }
}

function updateResult(data) {
  //output div
  var result = document.querySelector("#resultDiv");
  result.innerHTML = "";
  try {
    result.innerHTML = data.replace(/\n/g, '<br/>');
  } catch (e) {
    result.innerHTML = data;
  }
}

function updateCorpusArray() {
  corpusArray.push(document.querySelector('#corpus').value);
  updateCorpusInfo();
}

function resetCorpus() {
  corpusArray = [];
  updateCorpusInfo();
}

function updateCorpusInfo() {
  document.querySelector('#countCorpus').innerHTML = "Кол-во : "
      + corpusArray.length;
}
