package com.example.sparWeb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SparWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SparWebApplication.class, args);
	}

}

